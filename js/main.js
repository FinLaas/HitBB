import BackGround from './runtime/background'
import Ball from './player/ball'
import Music from './runtime/music'
import DataBus from './databus'

let ctx = canvas.getContext('2d')
let databus = new DataBus()

const screenWidth = window.innerWidth
const screenHeight = window.innerHeight

/**
 * 游戏主函数
 */
export default class Main {
  constructor() {
    // 维护当前requestAnimationFrame的id
    this.aniId = 0
    this.level = 0

    this.bg = new BackGround(ctx)
    this.music = new Music()
    this.touchHandler = this.touchEventHandler.bind(this)
    this.touchBeganHandler = this.touchBeganHandler.bind(this)
    canvas.addEventListener('touchend', this.touchHandler)
    canvas.addEventListener('touchstart', this.touchBeganHandler)
    this.bindLoop = this.loop.bind(this)
    this.aniId = window.requestAnimationFrame(
      this.bindLoop,
      canvas
    )
    this.restart()
  }

  restart() {
    databus.reset()

    this.whiteBall = new Ball(screenWidth / 2, screenHeight / 6 * 5, 30, "white")
    this.blackBall = new Ball(0, screenHeight / 2, 30, "grey")
    this.goal = new Ball(0, screenHeight / 6, 50, "pink")
    this.blackBall.reset()
    this.goal.reset()
    this.goal.resetRadius()
    
    // 清除上一局的动画
    //window.cancelAnimationFrame(this.aniId);
    
  }

  touchBeganHandler(e) {
    e.preventDefault()

    this.currentFrame = databus.frame
  }

  touchEventHandler(e) {
    e.preventDefault()

    if (databus.gameOver) {
      this.restart()
      databus.score = 0
    } else if (databus.ballHitted === false) {
      let x = e.changedTouches[0].clientX
      let y = e.changedTouches[0].clientY

      databus.ballHitted = true

      this.level = Math.min((databus.frame - this.currentFrame) / 10, 6)
      var ratio = screenHeight / 667 * (6 + this.level / 6 * 3)
      var diffX = this.whiteBall.x - x
      var diffY = this.whiteBall.y - y
      var velocityRatioX = diffX / Math.sqrt(diffX * diffX + diffY * diffY)
      var velocityRatioY = diffY / Math.sqrt(diffX * diffX + diffY * diffY)
      this.whiteBall.velocityX = velocityRatioX * ratio
      this.whiteBall.velocityY = velocityRatioY * ratio
    }    
  }

  // 游戏逻辑更新主函数
  update() {
    this.bg.update()
    this.whiteBall.update()
    this.blackBall.update()

    
    if (this.whiteBall.visible) {
      var x = this.whiteBall.x - this.blackBall.x
      var y = this.whiteBall.y - this.blackBall.y
      var z = Math.sqrt(x * x + y * y)
      if (z <= this.whiteBall.radius * 2) {
        this.whiteBall.visible = false

        var x1 = this.blackBall.x
        var y1 = this.blackBall.y
        var x2 = this.whiteBall.x
        var y2 = this.whiteBall.y
        var sqrt2 = Math.sqrt(this.whiteBall.velocityX * this.whiteBall.velocityX + this.whiteBall.velocityY * this.whiteBall.velocityY)
        var deep = Math.sqrt(x1 * x1 + y1 * y1) * sqrt2
        var head = x1 * x2 + y1 * y2
        var ratio = head / deep

        this.blackBall.velocityX = (this.whiteBall.x - this.blackBall.x) * sqrt2 / ratio
        this.blackBall.velocityY = (this.whiteBall.y - this.blackBall.y) * sqrt2 / ratio
      }
      if (this.whiteBall.isStop() && databus.ballHitted && databus.gameOver === false) {
        databus.gameOver = true
        this.music.playExplosion()
      }
    } else if (this.blackBall.isStop()) {
      //Black Ball Stop
      if (this.blackBall.distance(this.goal) <= (this.blackBall.radius + this.goal.radius)) {
        //success
        databus.score += 1
        console.log(databus.score)
        this.restart()
      } else if (databus.gameOver === false) {
        databus.gameOver = true
        this.music.playExplosion()
      }
    }
  } 

  render() {
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    this.bg.render(ctx)

    ctx.fillStyle = "grey"
    ctx.font = "20px Arial"
    ctx.fillText(databus.score, 20, 30)
    ctx.fillText(this.level, screenWidth - 40, 30)

    this.goal.render(ctx)
    this.whiteBall.render(ctx)
    this.blackBall.render(ctx)
  }

  // 实现游戏帧循环
  loop() {
    databus.frame++

    this.update()
    this.render()

    //if (databus.gameOver === false) {
      this.aniId = window.requestAnimationFrame(
        this.bindLoop,
        canvas
      )
    //}
    
  }
}
