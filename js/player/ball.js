const screenWidth = window.innerWidth

const miu = 0.01

export default class Ball {
  constructor (x = 0, y = 0, radius = 0, color = "white") {
    this.x = x
    this.y = y
    this.radius = radius
    this.color = color

    this.velocityX = 0
    this.velocityY = 0

    this.visible = true
  }

  update () {
    this.x -= this.velocityX
    this.y -= this.velocityY

    this.velocityX *= 0.986
    this.velocityY *= 0.986
  }

  reset () {
    this.x = Math.random() * (screenWidth / 2) + screenWidth / 4
  }

  resetRadius() {
    this.radius = (Math.random() + 1) * 30
  }

  render(ctx) {
    if (this.visible === false) return

    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fillStyle = this.color;
    ctx.fill();
  }

  distance(other) {
    var diffX = this.x - other.x
    var diffY = this.y - other.y
    return Math.sqrt(diffX * diffX + diffY * diffY)
  }

  isStop () {
    return this.velocityX <= 0.05 && this.velocityY <= 0.05
  }
}